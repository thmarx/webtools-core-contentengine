/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.definition.impl;

import java.util.HashMap;
import java.util.Map;

import com.thorstenmarx.webtools.modules.contentengine.definition.ConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import java.io.Serializable;

public class ContentDefinitionImpl implements ContentDefinition {


	private String id;
	private String targetUrl = null;
	/*
	 * Target für den HTML-Link des Banners
	 * Default = _self (Das Banner wird im selben Fenstern geöffnet)
	 */
	private String linkTarget = "_self";
	/*
	 * Der Titel der für den Link bei MouseOver angezeigt werden soll.
	 */
	private String linkTitle = null;

	/*
	 * Bedingungen für die Anzeige des Banners
	 */
	private Map<ConditionDefinitions, ConditionDefinition> conditionDefinitions = new HashMap<ConditionDefinitions, ConditionDefinition>();//new HashMap<ConditionDefinitions, ConditionDefinition>(ConditionDefinitions.class);
	/*
	 * Name of the product or null
	 */
	private final String type;
	
	private final Map<String, Serializable> content;
	
	
	public static ContentDefinition forType (final String type) {
		return new ContentDefinitionImpl(type);
	}
	
	private ContentDefinitionImpl (final String type) {
		this.type = type;
		this.content = new HashMap<>();
	}
	
	

	@Override
	public String getId() {
		return id;
	}
	@Override
	public void setId (String id) {
		this.id = id;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public boolean hasConditionDefinitions() {
		return this.conditionDefinitions.isEmpty();
	}

	@Override
	public boolean hasConditionDefinition(ConditionDefinitions key) {
		return this.conditionDefinitions.containsKey(key);
	}

	@Override
	public ConditionDefinition getConditionDefinition(ConditionDefinitions key) {
		return this.conditionDefinitions.get(key);
	}

	@Override
	public void addConditionDefinition(ConditionDefinitions key, ConditionDefinition value) {
		this.conditionDefinitions.put(key, value);
	}

	

	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContentDefinition)) {
			return false;
		}
		ContentDefinition other = (ContentDefinition)obj;
		return id.equals(other.getId());
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 23 * hash + (this.type != null ? this.type.hashCode() : 0);
		hash = 23 * hash + (this.id != null ? this.id.hashCode() : 0);
		hash = 23 * hash + (this.targetUrl != null ? this.targetUrl.hashCode() : 0);
		hash = 23 * hash + (this.linkTarget != null ? this.linkTarget.hashCode() : 0);
		hash = 23 * hash + (this.linkTitle != null ? this.linkTitle.hashCode() : 0);
		hash = 23 * hash + (this.conditionDefinitions != null ? this.conditionDefinitions.hashCode() : 0);
		return hash;
	}

	@Override
	public void addContent(final String key, final Serializable value) {
		this.content.put(key, value);
	}

	@Override
	public void removeContent(final String key) {
		this.content .remove(key);
	}

	@Override
	public void clearContent() {
		this.content.clear();
	}

	@Override
	public Serializable getContent(String key) {
		return content.get(key);
	}
}

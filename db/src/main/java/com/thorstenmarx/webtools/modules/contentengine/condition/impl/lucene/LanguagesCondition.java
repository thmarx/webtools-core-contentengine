/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.LanguageConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.StateConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.model.State;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

public class LanguagesCondition implements Condition<Document, BooleanQuery.Builder> {

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.locale() == null) {
			return;
		}
		String language = request.locale().getLanguage();
		
		BooleanQuery.Builder query = new BooleanQuery.Builder();
		
		BooleanQuery.Builder temp = new BooleanQuery.Builder();
		temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_LANGUAGE, language.toLowerCase())), Occur.SHOULD);
		
		query.add(temp.build(), Occur.MUST);
		
		mainQuery.add(query.build(), Occur.MUST);
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		
		LanguageConditionDefinition stDef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.LANGUAGE)) {
			stDef = (LanguageConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.LANGUAGE);
		}
		
		if (stDef != null && stDef.getLanguages().size() > 0) {
			List<String> list = stDef.getLanguages();
			for (String language : list) {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_LANGUAGE, language.toLowerCase(), Field.Store.NO));
			}
		} else {
//			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_LANGUAGE, ContentEngineConstants.CONTENT_LANGUAGE_ALL, Field.Store.NO));
		}
	}

}

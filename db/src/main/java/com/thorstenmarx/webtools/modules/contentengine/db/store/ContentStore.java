/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db.store;

import java.io.IOException;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import java.util.Optional;

public interface ContentStore {
	/**
	 * Open the AdStore
	 * @throws IOException
	 */
	public void open() throws IOException;
	/**
	 * Close the AdStore
	 * @throws IOException
	 */
	public void close() throws IOException;
	/**
	 * Reopen the AdStore
	 * only needed by some implementations
	 * @throws IOException
	 */
	public void reopen () throws IOException;
	/**
	 * Add AdDefinition to the AdStore
	 * 
	 * @param definition
	 * @throws IOException
	 */
	public void add (ContentDefinition definition) throws IOException;
	/**
	 * Deletes AdDefinition from AdStore
	 * @param id
	 * @throws IOException
	 */
	public void delete (String id) throws IOException;
	/**
	 * Returns an AdDefintion from AdStore
	 * @param id
	 * @return
	 */
	public Optional<ContentDefinition> get (String id);
	/**
	 * Search for AdDefinitions
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public List<ContentDefinition> search (ContentRequest request) throws IOException;
	/**
	 * AdDefinition count
	 * @return
	 */
	public int size () throws IOException;
	/**
	 * Remove all AdDefinitions from AdStore
	 * @throws IOException
	 */
	public void clear () throws IOException;
}

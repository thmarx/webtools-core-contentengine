/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.util.BytesRef;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.TimeConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.TimeConditionDefinition.Period;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Bedingung bzgl. des Zeitraums in dem das Banner angezeigt werden soll
 * z.B. zwischen 23 und 4 Uhr 
 * 
 * 04.07.2011:
 * Nach der Anpassung können im Banner nun Perioden angegeben werden, wann ein Banner
 * angezeigt werden soll 
 * zwischen 8 und 10 Uhr und zwischen 12 und 14 Uhr 
 * 
 * @author tmarx
 *
 */
public class TimeCondition implements Condition<Document, BooleanQuery.Builder> {

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		BooleanQuery.Builder query = null;
		
		if (request.time() != null) { 
			
			BooleanQuery.Builder tempquery = new BooleanQuery.Builder();
			
			/*
			 * Prefix der Indizieren beachten.
			 */
			for (int i = 0; i < TimeConditionDefinition.MAX_PERIOD_COUNT; i++) {
				query = new BooleanQuery.Builder();
				
				BooleanQuery.Builder temp = new BooleanQuery.Builder();
				TermRangeQuery tQuery = new TermRangeQuery(ContentEngineConstants.CONTENT_TIME_FROM + i, new BytesRef("0000"), new BytesRef(request.time()), true, true);
				temp.add(tQuery, Occur.SHOULD);
//				temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_TIME_FROM + i, ContentEngineConstants.CONTENT_TIME_ALL)), Occur.SHOULD);
				
				query.add(temp.build(), Occur.MUST);
				
				temp = new BooleanQuery.Builder();
				tQuery = new TermRangeQuery(ContentEngineConstants.CONTENT_TIME_TO + i, new BytesRef(request.time()), new BytesRef("2500"), true, true);
				temp.add(tQuery, Occur.SHOULD);
//				temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_TIME_TO + i, ContentEngineConstants.CONTENT_TIME_ALL)), Occur.SHOULD);
				
				query.add(temp.build(), Occur.MUST);
				
				tempquery.add(query.build(), Occur.SHOULD);
			}	
			mainQuery.add(tempquery.build(), Occur.MUST);
		}
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		TimeConditionDefinition tdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.TIME)) {
			tdef = (TimeConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.TIME);
		}
		
		if (tdef != null && !tdef.getPeriods().isEmpty()) {
			/*
			 * 	Um die Paare von/zu der Perioden zu erhalten, werden die jeweilige Felder geprefixt.
			 *  Dadurch können bei der Suche die einzelnen Perioden unterschieden werden
			 */
			int count = 0;
			for (Period p : tdef.getPeriods()) {
				if (p.getFrom() != null) {
					bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_TIME_FROM + count, p.getFrom(), Field.Store.NO));
				} else {
					bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_TIME_FROM + count, ContentEngineConstants.CONTENT_TIME_ALL, Field.Store.NO));
				}
				
				if (p.getTo() != null) {
					bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_TIME_TO + count, p.getTo(), Field.Store.NO));
				} else {
					bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_TIME_TO + count, ContentEngineConstants.CONTENT_TIME_ALL, Field.Store.NO));
				}
				count++;
			}
		} else {
			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_TIME_FROM + 0, ContentEngineConstants.CONTENT_TIME_ALL, Field.Store.NO));
			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_TIME_TO + 0, ContentEngineConstants.CONTENT_TIME_ALL, Field.Store.NO));
		}
	}

}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.Keyword;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.KeywordConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;


/**
 * Keyword-Bedingung
 * 
 * Die Klasse erweitert das Document und das Query
 * 
 * @author tmarx
 *
 */
public class KeywordCondition implements Condition<Document, BooleanQuery.Builder> {

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.keywords() == null || request.keywords().isEmpty()) {
			return;
		}
		
		BooleanQuery.Builder query = new BooleanQuery.Builder();
		
		BooleanQuery.Builder temp = new BooleanQuery.Builder();
		
		// keywords einfügen
		for (String k : request.keywords()) {
			temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_KEYWORD, k)), Occur.SHOULD);
		}
		
		query.add(temp.build(), Occur.MUST);
		mainQuery.add(query.build(), Occur.MUST);
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		
		KeywordConditionDefinition kdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.KEYWORD)) {
			kdef = (KeywordConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.KEYWORD);
		}
		
		if (kdef != null && kdef.getKeywords().size() > 0) {
			// keywords im Dokument speichern
			List<Keyword> kws = kdef.getKeywords();
			for (Keyword k : kws) {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_KEYWORD, k.word, Field.Store.NO));
			}
		} else {
			/*
			 * für alle Banner ohne angegebenem Keyword wird das default ALL-Keyword gesetzt
			 */
//			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_KEYWORD, ContentEngineConstants.CONTENT_KEYWORD_ALL, Field.Store.NO));
		}
	}

}

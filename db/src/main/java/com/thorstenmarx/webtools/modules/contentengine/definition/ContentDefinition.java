/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.definition;

import java.io.Serializable;

import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;

public interface ContentDefinition extends Serializable {
	/**
	 * ID des Inhalts
	 * @return
	 */
	public String getId ();
	public void setId (String id);
	
	public void addContent (String key, Serializable value);
	public void removeContent (String key);
	public void clearContent ();
	public Serializable getContent (String key);
	
	/**
	 * Liefert den Type (Image, Text)
	 * @return
	 */
	public String getType ();
	
	public boolean hasConditionDefinitions ();
	public boolean hasConditionDefinition (ConditionDefinitions key);
	public ConditionDefinition getConditionDefinition (ConditionDefinitions key);
	public void addConditionDefinition (ConditionDefinitions key, ConditionDefinition value);
}
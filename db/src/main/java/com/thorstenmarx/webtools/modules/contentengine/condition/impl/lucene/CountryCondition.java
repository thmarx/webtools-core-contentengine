/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;


import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.CountryConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.model.Country;
import java.util.Locale;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Bedingung für das Land in dem ein Banner angezeigt werden soll
 * 
 * @author tmarx
 *
 */
public class CountryCondition implements Condition <Document, BooleanQuery.Builder> {

	private static final long serialVersionUID = -7216929200604017718L;

	
	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.country() == null) {
			return;
		}
		
		String country = request.country().getCode();

		BooleanQuery.Builder query = new BooleanQuery.Builder();
		
		BooleanQuery.Builder temp = new BooleanQuery.Builder();
		temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_COUNTRY, country.toLowerCase(Locale.getDefault()))), Occur.SHOULD);
		
		query.add(temp.build(), Occur.MUST);
		mainQuery.add(query.build(), Occur.MUST);
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		
		CountryConditionDefinition cdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.COUNTRY)) {
			cdef = (CountryConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.COUNTRY);
		}
		
		if (cdef != null && cdef.getCountries().size() > 0) {
			List<Country> list = cdef.getCountries();
			list.stream().forEach((c) -> {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_COUNTRY, c.getCode().toLowerCase(Locale.getDefault()), Field.Store.NO));
			});
		} else {
//			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_COUNTRY, Country.ALL.getCode(), Field.Store.NO));
		}
	}

}

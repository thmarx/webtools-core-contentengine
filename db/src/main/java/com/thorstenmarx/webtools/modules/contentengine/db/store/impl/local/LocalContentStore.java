/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
//import org.apache.lucene.search.NRTManager;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.NRTCachingDirectory;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.utils.LuceneDocumentHelper;
import com.thorstenmarx.webtools.modules.contentengine.utils.LuceneQueryHelper;
import java.nio.file.Paths;
import org.apache.lucene.search.SearcherFactory;
import com.thorstenmarx.webtools.modules.contentengine.db.store.ContentStore;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import static com.thorstenmarx.webtools.modules.contentengine.utils.Objects2.*;
import java.util.Optional;
import org.iq80.leveldb.DB;
import org.iq80.leveldb.DBFactory;
import org.iq80.leveldb.DBIterator;
import org.iq80.leveldb.Options;
import static org.iq80.leveldb.impl.Iq80DBFactory.*;
import org.iq80.leveldb.impl.Iq80DBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalContentStore implements ContentStore {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalContentStore.class);

	public static final String CONFIG_DATADIR = "datadir";

	private ContentDBImpl addb = null;

	// Lucene
	private Directory index = null;
	private IndexWriter writer = null;
	// private TrackingIndexWriter trackingWriter = null;

	// private NRTManager nrt_manager = null;
	// private ControlledRealTimeReopenThread<IndexSearcher> nrtThread = null;
	private SearcherManager nrt_manager;
	private NRTCachingDirectory nrt_index;

	private DB db;

	public LocalContentStore(ContentDBImpl db) {
		this.addb = db;
	}

	@Override
	public void open() throws IOException {

		if (!addb.manager.getContext().getConfiguration().containsKey(CONFIG_DATADIR)) {
			throw new IOException("data directory can not be empty");
		}

		String dir = (String) addb.manager.getContext().getConfiguration().get(CONFIG_DATADIR);
		if (!dir.endsWith("/") || !dir.endsWith("\\")) {
			dir += "/";
		}
		File temp = new File(dir + "store");
		if (!temp.exists()) {
			temp.mkdirs();
		}
		temp = new File(dir + "index");
		if (!temp.exists()) {
			temp.mkdirs();
		}
		// create lucene index directory
		index = FSDirectory.open(Paths.get(temp.toURI()));
		nrt_index = new NRTCachingDirectory(index, 5.0, 60.0);

		openPersistence(dir + "store");

		IndexWriterConfig config = new IndexWriterConfig(new KeywordAnalyzer());
		// CREATE_OR_APPEND
		config.setOpenMode(OpenMode.CREATE_OR_APPEND);
		writer = new IndexWriter(nrt_index != null ? nrt_index : index, config);

		nrt_manager = new SearcherManager(writer, true, true, new SearcherFactory());

	}

	private void openPersistence(final String dir) throws IOException {
		Options options = new Options();
		options.createIfMissing(true);
		DBFactory factory = new Iq80DBFactory();

		db = factory.open(new File(dir), options);
	}

	@Override
	public void close() throws IOException {
		db.close();

		this.writer.commit();
		this.writer.close();
		// nrt_manager.close();
		this.index.close();
	}

	@Override
	public void reopen() throws IOException {
		this.writer.forceMerge(1); // optimize();
		this.writer.commit();

		nrt_manager.maybeRefresh();
	}

	@Override
	public void add(ContentDefinition definition) throws IOException {
		// add index
		Document doc = LuceneDocumentHelper.getInstance().getBannerDocument(definition, this.addb);
		this.writer.addDocument(doc);

		db.put(bytes(definition.getId()), toByteArray(definition));
	}

	@Override
	public void delete(String id) throws IOException {
		// add to index
		this.writer.deleteDocuments(new Term(ContentEngineConstants.CONTENT_ID, id));
		// add to db
		this.db.delete(bytes(id));
	}

	@Override
	public Optional<ContentDefinition> get(String id) {
		try {
			final byte[] bytes = this.db.get(bytes(id));
			if (bytes == null || bytes.length == 0) {
				return Optional.empty();
			}
			return Optional.ofNullable(toObject(bytes, ContentDefinition.class));
		} catch (IOException | ClassNotFoundException ex) {
			LOGGER.error("", ex);
		}

		return Optional.empty();
	}

	@Override
	public List<ContentDefinition> search(ContentRequest request) throws IOException {
		IndexSearcher searcher = nrt_manager.acquire();
		List<ContentDefinition> result = new ArrayList<>();
		try {
			// Collector für die Banner
			ContentCollector collector = new ContentCollector(searcher.getIndexReader().numDocs());

			// MainQuery
			BooleanQuery.Builder mainQueryBuilder = new BooleanQuery.Builder();
			// Query für den/die BannerTypen
			BooleanQuery.Builder typeQueryBuilder = new BooleanQuery.Builder();
			for (String type : request.types()) {
				TermQuery tq = new TermQuery(new Term(ContentEngineConstants.CONTENT_TYPE, type));
				typeQueryBuilder.add(tq, Occur.SHOULD);
			}
			mainQueryBuilder.add(typeQueryBuilder.build(), Occur.MUST);

			// Query für die Bedingungen unter denen ein Banner angezeigt werden soll
			Query cq = LuceneQueryHelper.getInstance().getConditionalQuery(request, this.addb);
			if (cq != null) {
				mainQueryBuilder.add(cq, Occur.MUST);
			}

			BooleanQuery mainQuery = mainQueryBuilder.build();
			LOGGER.debug(mainQuery.toString());
			System.out.println(mainQuery.toString());

			searcher.search(mainQuery, collector);

			BitSet hits = collector.getHits();
			// Ergebnis
			for (int i = hits.nextSetBit(0); i != -1; i = hits.nextSetBit(i + 1)) {
				Document doc = searcher.doc(i);
				final Optional<ContentDefinition> contentDefinition = addb.get(doc.get(ContentEngineConstants.CONTENT_ID));
				if (!contentDefinition.isEmpty()) {
					result.add(contentDefinition.get());
				}
			}
		} finally {
			nrt_manager.release(searcher);
		}

		return result;
	}

	@Override
	public int size() throws IOException {
		IndexSearcher searcher = null;
		try {
			searcher = nrt_manager.acquire();
			return searcher.getIndexReader().numDocs();
		} finally {
			try {
				if (searcher != null) {
					nrt_manager.release(searcher);
				}
			} catch (IOException e) {
				LOGGER.error("", e);
			}
		}
	}

	@Override
	public void clear() throws IOException {

		this.writer.deleteAll();
		try {
			this.reopen();

			try (DBIterator iterator = db.iterator()) {
				for (iterator.seekToFirst(); iterator.hasNext(); iterator.next()) {
					db.delete(iterator.peekNext().getKey());
				}
			}

		} catch (IOException ioe) {
			this.writer.rollback();
			throw ioe;
		}
	}

}

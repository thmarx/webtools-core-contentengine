/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.definition.condition;


import java.util.ArrayList;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.definition.ContentSlot;
import com.thorstenmarx.webtools.modules.contentengine.definition.ConditionDefinition;

/**
 * Steuerung auf welchen Seiten das Banner angezeigt werden soll
 * 
 * @author tmarx
 *
 */
public class ContentSlotConditionDefinition implements ConditionDefinition {
	
	private final List<ContentSlot> slots = new ArrayList<>();
	
	public ContentSlotConditionDefinition () {
		
	}
	
	/**
	 * ID der Slots auf denen der Inhalt angezeigt werden soll
	 * @return 
	 */
	public List<ContentSlot> getSlots() {
		return this.slots;
	}
	public void addSlot (ContentSlot slot) {
		this.slots.add(slot);
	}
}

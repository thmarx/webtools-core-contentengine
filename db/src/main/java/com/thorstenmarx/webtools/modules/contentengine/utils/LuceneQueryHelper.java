/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.utils;

import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;

import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;


public class LuceneQueryHelper {
	private static LuceneQueryHelper INSTANCE = null;
	
	private LuceneQueryHelper() {
	}
	
	public static synchronized LuceneQueryHelper getInstance () {
		if (INSTANCE == null) {
			INSTANCE = new LuceneQueryHelper();
		}
		return INSTANCE;
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public Query getConditionalQuery (ContentRequest request, ContentDBImpl addb) {
		
		BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
		
		for (Condition condition : addb.manager.getConditions()) {
			condition.addQuery(request, queryBuilder);
		}
		
		BooleanQuery query = queryBuilder.build();
		if (query.clauses() == null || query.clauses().isEmpty()){
			return null;
		}
		return query;
	}
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.BooleanQuery;

import com.google.common.base.Predicate;
import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.condition.Filter;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.DistanceConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Condition zum Filtern von Bannern, die nur in einem bestimmten Radius um
 * eine Geo-Koordinate angezeigt werden sollen
 * 
 * @author tmarx
 *
 */
public class DistanceCondition implements Condition<Document, BooleanQuery.Builder>, Filter {

	@Override
	public Predicate<ContentDefinition> getFilterPredicate(final ContentRequest request) {
		Predicate<ContentDefinition> predicate = new Predicate<ContentDefinition>() {
			@Override
			public boolean apply(ContentDefinition type) {
				
				if (!type.hasConditionDefinition(ConditionDefinitions.DISTANCE)) {
					return true;
				}
				DistanceConditionDefinition dcdef = (DistanceConditionDefinition) type.getConditionDefinition(ConditionDefinitions.DISTANCE);
				if (request.geoLocation() == null) {
					/*
					 * Der Request liefert keine Position, dann wird das Banner ebenfalls angezeigt
					 * 
					 * TODO: evtl. könnte man das noch über einen Parameter steuern
					 */
					return true;
				}
				
				/*
				 * Positionen für Banner und Request
				 */
				LatLng bannerPOS = new LatLng(dcdef.getGeoLocation().getLatitude(), dcdef.getGeoLocation().getLongitude());
				LatLng requestPOS = new LatLng(request.geoLocation().getLatitude(), request.geoLocation().getLongitude());
				/*
				 * Distance zwischen Banner und Request
				 */
				double distance = LatLngTool.distance(bannerPOS, requestPOS, LengthUnit.KILOMETER);
				
				/*
				 * Wird der Banner angegebene Radius nicht überschritten, wird das Banner angezeigt, ansonsten nicht
				 */
				if (dcdef.getGeoRadius() >= distance) {
					return true;
				}
				
				return false;
			}
		};
		return predicate;
	}

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		// aktuell wird kein Radius Query unterstützt
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		// aktuell wird kein Radius Query unterstützt
	}

}

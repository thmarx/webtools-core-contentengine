/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.definition;

import java.io.Serializable;

import com.google.common.base.Strings;


/**
 * The AdSlot is the combination of a page and an unique place
 * 
 * @author thmarx
 *
 */
public class ContentSlot implements Serializable {
	
	private final String site;
	private final String place;
	
	private String toString_value = "";
	
	public ContentSlot (String site, String place) {
		this.site = site;
		this.place = place;
		
		this.toString_value = new StringBuilder(site).append("-").append(place).toString();
	}
	
	public String getSite() {
		return site;
	}

	public String getPlace() {
		return place;
	}

	@Override
	public String toString() {
//		return Base16.encode(site) + "-" + Base16.encode(place);
		return toString_value;
	}
	
	public static ContentSlot fromString (String uuid) throws IllegalArgumentException{
		if (Strings.isNullOrEmpty(uuid)) {
            throw new IllegalArgumentException("Invalid AdUUID string: " + uuid);
        }
		String[] components = uuid.split("-");
        if (components.length != 2) {
            throw new IllegalArgumentException("Invalid AdUUID string: " + uuid);
        }
        
//        AdSlot aduuid = new AdSlot(Base16.decode(components[0]), Base16.decode(components[1]));
        ContentSlot aduuid = new ContentSlot(components[0], components[1]);
        
        return aduuid;
	}


	public static void main (String []args) throws Exception {
		ContentSlot uuid = new ContentSlot("1", "2");
		System.out.println(uuid.toString());
		ContentSlot uuid2 = ContentSlot.fromString(uuid.toString());
		
		System.out.println(uuid2.getSite());
		System.out.println(uuid2.getPlace());
	}
}

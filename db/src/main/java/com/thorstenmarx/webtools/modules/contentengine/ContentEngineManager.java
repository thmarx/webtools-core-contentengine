/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.nonblocking.NonBlockingContentDB;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;

public class ContentEngineManager {

	private final ContentDBImpl adDB;
	
	private final ContentEngineContext context;
	
	private final List<Condition<?, ?>> conditions = new ArrayList<>();
	
	private ExecutorService executorService = null;
	
	public static Builder builder () {
		return new Builder();
	}
	
	private ContentEngineManager (boolean blocking, boolean closeExecutorService, ContentEngineContext context) {
		this.context = context;
		if (blocking) {
			this.adDB = new ContentDBImpl(this);
		} else {
			this.adDB = new NonBlockingContentDB(this);
		}
		
		
		/*
		 * should the executor service be closed after shutdown
		 * 
		 * maybe it is managed outside of the AdDBManager so it should not be closed here
		 */
		if (executorService != null && closeExecutorService) {
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					executorService.shutdownNow();
				}
			});
		}
	}
	
	public ExecutorService getExecutorService () {
		return this.executorService;
	}
	
	public ContentDBImpl getAdDB () {
		return adDB;
	}
	
	public List<Condition<?, ?>> getConditions () {
		return conditions;
	}
	
	public ContentEngineContext getContext () {
		return context;
	}
	
	static public class Builder {
		private boolean blocking = true;
		private ExecutorService executorService = null;
		private boolean closeExecutorService = false;
		private Mode mode = Mode.MEMORY;
		private ContentEngineContext context = null;
		
		private Builder () {
			
		}
		
		public Builder blocking (boolean blocking) {
			this.blocking = blocking;
			return this;
		}
		public Builder context (ContentEngineContext context) {
			this.context = context;
			return this;
		}
		public Builder mode (Mode mode) {
			this.mode = mode;
			return this;
		}
		
		public Builder closeExecutorService (boolean closeExecutorService) {
			this.closeExecutorService = closeExecutorService;
			return this;
		}
		
		public Builder executorService (ExecutorService executorService) {
			this.executorService = executorService;
			return this;
		}
		
		public ContentEngineManager build () {
			if (this.context == null) {
				this.context = new ContentEngineContext();
			}
			this.context.mode = mode;
			ContentEngineManager manager = new ContentEngineManager(blocking, closeExecutorService, this.context);
			
			
			// Default Conditions
			if (Mode.LOCAL.equals(mode) || Mode.MEMORY.equals(mode)) {
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.CountryCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.LanguagesCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.StateCondition());
//				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.DateCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.DayCondition());
//				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.TimeCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.KeywordCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.KeyValueCondition(manager.adDB));
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.SiteCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.ContentSlotCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.DistanceCondition());
				manager.conditions.add(new com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene.ValidFromToCondition());
			}
			
			if (!blocking && executorService == null) {
				executorService = Executors.newFixedThreadPool(1);
			}
			manager.executorService = executorService;
			
			return manager;
		}
	}
}

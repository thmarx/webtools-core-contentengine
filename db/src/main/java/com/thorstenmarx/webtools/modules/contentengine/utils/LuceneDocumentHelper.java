/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.utils;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

public class LuceneDocumentHelper {
	private static LuceneDocumentHelper INSTANCE = null;
	
	private LuceneDocumentHelper (){
	}
	
	public static synchronized LuceneDocumentHelper getInstance () {
		if (INSTANCE == null){
			INSTANCE = new LuceneDocumentHelper();
		}
		return INSTANCE;
	}
	
	public Document getBannerDocument (ContentDefinition banner, ContentDBImpl addb) {
		Document doc = new Document();
		doc.add(new StringField(ContentEngineConstants.CONTENT_ID, String.valueOf(banner.getId()), Field.Store.YES));
		doc.add(new StringField(ContentEngineConstants.CONTENT_TYPE, banner.getType(), Field.Store.NO));
		
		doc = addConditions(banner, doc, addb);
		
		return doc;
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	private Document addConditions (ContentDefinition banner, Document doc, ContentDBImpl addb) {
		
		for (Condition condition : addb.manager.getConditions()) {
			condition.addFields(doc, banner);
		}
		
		return doc;
	}
}

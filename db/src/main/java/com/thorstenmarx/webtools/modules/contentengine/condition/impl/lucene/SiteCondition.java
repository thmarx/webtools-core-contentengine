/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.SiteConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;


public class SiteCondition implements Condition<Document, BooleanQuery.Builder> {

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.site() == null) {
			return;
		}
		
		
		BooleanQuery.Builder query = new BooleanQuery.Builder();
		
		BooleanQuery.Builder temp = new BooleanQuery.Builder();
		
		// Seite einfügen
		temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_SITE, request.site())), Occur.SHOULD);
		
		query.add(temp.build(), Occur.MUST);
		mainQuery.add(query.build(), Occur.MUST);
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		
		SiteConditionDefinition sdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.SITE)) {
			sdef = (SiteConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.SITE);
		}
		
		if (sdef != null && !sdef.getSites().isEmpty()) {
			// Sites im Dokument speichern
			List<String> sites = sdef.getSites();
			for (String site : sites) {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_SITE, site, Field.Store.NO));
			}
		} else {
			/*
			 * Banner, die keine Einschräkung auf eine spezielle Seite haben
			 */
//			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_SITE, ContentEngineConstants.CONTENT_SITE_ALL, Field.Store.NO));
		}
	}

}

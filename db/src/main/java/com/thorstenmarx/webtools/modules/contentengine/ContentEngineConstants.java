/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine;

public final class ContentEngineConstants {

	private ContentEngineConstants() {
	}
	
	// DB fields
	public static final String CONTENT_ID = "content_id";
	public static final String CONTENT_TYPE = "content_type";
	
	public static final String CONTENT_VALID_FROM = "content_valid_from";
	public static final String CONTENT_VALID_TO = "content_valid_to";
	public static final String CONTENT_VALID_ALL = "all";
	public static final String CONTENT_TIME_FROM = "content_time_from";
	public static final String CONTENT_TIME_TO = "content_time_to";
	public static final String CONTENT_TIME_ALL = "all";
	public static final String CONTENT_DATE_FROM = "content_date_from";
	public static final String CONTENT_DATE_TO = "content_date_to";
	public static final String CONTENT_DATE_ALL = "all";
	public static final String CONTENT_DAY = "content_day";
//	public static final String CONTENT_DAY_ALL = "0";
	public static final String CONTENT_STATE = "content_state";
//	public static final String CONTENT_STATE_ALL = "0";
	public static final String CONTENT_COUNTRY = "content_country";
//	public static final String CONTENT_COUNTRY_ALL = "all";
	public static final String CONTENT_LANGUAGE = "content_language";
//	public static final String CONTENT_LANGUAGE_ALL = "all";
	public static final String CONTENT_DEFAULT = "content_default";
	
	public static final String CONTENT_KEYWORD = "content_keyword";
//	public static final String CONTENT_KEYWORD_ALL = "all";
	public static final String CONTENT_KEYVALUE = "content_keyvalue";
//	public static final String CONTENT_KEYVALUE_ALL = "all";
	public static final String CONTENT_SITE = "content_site";
//	public static final String CONTENT_SITE_ALL = "all";
	
	public static final String CONTENT_SLOT = "content_slot";
//	public static final String CONTENT_SLOT_ALL = "all";
	
	public static final String ADDB_AD_SITE_EXCLUDE = "content_site_exclude";
	// TODO: Felder für das Targeting
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local;

import java.io.IOException;
import java.util.BitSet;
import org.apache.lucene.index.LeafReaderContext;

import org.apache.lucene.search.Collector;
import org.apache.lucene.search.ScoreMode;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.SimpleCollector;

public class ContentCollector extends SimpleCollector {

	private Scorer scorer;
	
	private int docbase = 0;

	private BitSet hits = null;

	public ContentCollector(int size) {
		hits = new BitSet(size);
	}

	@Override
	protected void doSetNextReader(LeafReaderContext context) throws IOException {
		super.doSetNextReader(context); //To change body of generated methods, choose Tools | Templates.
		this.docbase = context.docBase;
	}
	
	

	// simply print docId and score of every matching document
	@Override
	public void collect(int doc) throws IOException {
		hits.set(docbase + doc);
	}
	

	
	public BitSet getHits () {
		return hits;
	}

	@Override
	public ScoreMode scoreMode() {
		return ScoreMode.COMPLETE_NO_SCORES;
	}

}

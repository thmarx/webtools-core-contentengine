/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;

public class ContentEngineContext {
	public Mode mode = Mode.MEMORY;
	
//	public String datadir = null;
	
	public Set<String> validKeys = new HashSet<>();
	
	public Map<String, Object> configuration = new HashMap<>();

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	
//	public String getDatadir() {
//		return datadir;
//	}
//
//	public void setDatadir(String datadir) {
//		this.datadir = datadir;
//	}

	public Set<String> getValidKeys() {
		return validKeys;
	}

	public void setValidKeys(Set<String> validKeys) {
		this.validKeys = validKeys;
	}

	public Map<String, Object> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, Object> configuration) {
		this.configuration = configuration;
	}
	
	
}

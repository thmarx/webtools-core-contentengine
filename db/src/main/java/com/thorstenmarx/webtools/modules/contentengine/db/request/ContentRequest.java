/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db.request;


import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.thorstenmarx.webtools.modules.contentengine.enums.Day;
import com.thorstenmarx.webtools.modules.contentengine.model.Country;
import com.thorstenmarx.webtools.modules.contentengine.model.State;
import com.thorstenmarx.webtools.modules.contentengine.utils.geo.GeoLocation;

public class ContentRequest {
	/*
	 *  Anzahl der Banner, die geladen werden soll
	 *  
	 *  -1 = alle
	 */
	private int count = -1;
	
	private List<String> types = new ArrayList<>();
	
	/*
	 * Conditions
	 * Die vor dem Aufruf gesetzt werden können
	 */
	private Day day = null;
	/*
	 * Das Bundesland, in dem sich der Aufrufer befindet 
	 */
	private State state = null;
	/*
	 * Das Land in dem sich der Aufrufer befindet
	 */
	private Country country = null;
	
	private Locale locale = null;
	/*
	 * Die Zeit des Aufrufers
	 */
	private String time = null;
	/*
	 * Das Datum des Aufrufers
	 */
	private String date = null;
	/*
	 * Keywords für die Banner
	 */
	private List<String> keywords = new ArrayList<>();
	/*
	 * Key-Values
	 */
	private Multimap<String, String> keyValues = MultimapBuilder.hashKeys().arrayListValues().build();
	
	/*
	 * ID der Seite auf der das Banner eingebunden wird
	 */
	private String site = null;

	/*
	 * Geo-Position für die ein Banner angezeigt werden soll
	 */
	private GeoLocation geoLocation = null;
	/*
	 * Radius für gültige Banner um die GeoPosition
	 */
	private int radius;
	
	/*
	 * id of the adslot
	 */
	private String adSlot = null;

	public ContentRequest () {
		
	}

	public String adSlot() {
		return adSlot;
	}

	public ContentRequest adSlot(String adSlot) {
		this.adSlot = adSlot;
		return this;
	}

	public GeoLocation geoLocation() {
		return geoLocation;
	}
	public ContentRequest geoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
		return this;
	}

	public String site () {
		return this.site;
	}
	public ContentRequest site (String site) {
		this.site = site;
		return this;
	}
	
	public List<String> keywords() {
		return keywords;
	}
	
	public ContentRequest keywords(List<String> keywords) {
		this.keywords = keywords;
		return this;
	}
	
	public void addKeyValue (final String key, final String value) {
		keyValues.put(key, value);
	}
	
	public Multimap<String, String> keyValues () {
		return Multimaps.unmodifiableMultimap(keyValues);
	}

	public Country country() {
		return country;
	}

	public ContentRequest country(Country country) {
		this.country = country;
		return this;
	}

	
	
	public Locale locale() {
		return locale;
	}

	public ContentRequest locale(Locale locale) {
		this.locale = locale;
		return this;
	}

	public int count() {
		return count;
	}

	public ContentRequest count(int count) {
		this.count = count;
		return this;
	}

	public List<String> types() {
		return types;
	}

	public ContentRequest types(List<String> types) {
		this.types = types;
		return this;
	}

	public Day day() {
		return day;
	}

	public ContentRequest day(Day day) {
		this.day = day;
		return this;
	}

	public State state() {
		return state;
	}

	public ContentRequest state(State state) {
		this.state = state;
		return this;
	}

	public String time() {
		return time;
	}

	public ContentRequest time(String time) {
		this.time = time;
		return this;
	}

	public String date() {
		return date;
	}

	public ContentRequest date(String date) {
		this.date = date;
		return this;
	}

	
	public String cacheKey() {
		return "ContentRequest{" + "types=" + types + ", day=" + day + ", time=" + time + ", date=" + date + ", keyValues=" + keyValues + '}';
	}
	
	
	
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.DayConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.enums.Day;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Bedingung die angiebt, an welchen Tage der Woche ein Banner angezeigt werden soll
 *
 * 0 = an allen Tagen 
 * 1 = Montag 
 * 2 = Dienstag 
 * 3 = Mittwoch 
 * 4 = Donnerstag 
 * 5 = Freitag 
 * 6 = Samstag 
 * 7 = Sonntag
 *
 * @author thmarx
 *
 */
public class DayCondition implements Condition<Document, BooleanQuery.Builder> {

	private static final long serialVersionUID = 5608846584544996300L;

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.day() == null) {
			return;
		}
		int day = request.day().getDay();

		BooleanQuery.Builder query = new BooleanQuery.Builder();

		BooleanQuery.Builder temp = new BooleanQuery.Builder();
		temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_DAY, String.valueOf(day))), Occur.SHOULD);

		query.add(temp.build(), Occur.MUST);

		mainQuery.add(query.build(), Occur.MUST);
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		DayConditionDefinition ddef = null;

		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.DAY)) {
			ddef = (DayConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.DAY);
		}

		if (ddef != null && ddef.getDays().size() > 0) {
			List<Day> list = ddef.getDays();
			list.stream().forEach((day) -> {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_DAY, String.valueOf(day.getDay()), Field.Store.NO));
			});
		}
	}

}

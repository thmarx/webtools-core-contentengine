/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db;


import java.io.IOException;
import java.util.List;



import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;
import com.thorstenmarx.webtools.modules.contentengine.utils.ConditionHelper;
import com.thorstenmarx.webtools.modules.contentengine.db.store.ContentStore;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BDB - BannerDatenbank
 * 
 * 
 * @author thorsten
 */
public class ContentDBImpl implements ContentDB {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(ContentDBImpl.class);
	
//    private AdDBStore adStore;
//    private AdDBIndex adIndex = null;
	private ContentStore store;
    
    public final ContentEngineManager manager;
    
    private boolean open = false;
    
	public ContentDBImpl(ContentEngineManager manager) {
		this.manager = manager;
	}

	public void open() throws IOException {
		if (this.open) {
			return;
		}
		if (manager.getContext().mode.equals(Mode.LOCAL)) {
			store = new LocalContentStore(this);
//			store = new LocalLuceneAdStore(this);
		}
		
		
		store.open();
		
		this.open = true;
	}
	
	public void reopen () throws IOException {
		store.reopen();
	}
	
	@Override
	public void clear () throws IOException {
		store.clear();
	}
	
	public void close() throws IOException {
		this.store.close();
		
		this.open = false;
	}
	
	/**
	 * Schreibt ein Banner in den Index und in die Datenbank
	 * 
	 * @param banner
	 * @throws IOException
	 */
	@Override
	public void add (ContentDefinition banner) throws IOException {
		store.add(banner);
	}
	
	/**
	 * Löscht ein Banner aus dem Index und der Datenbank
	 * @param id
	 * @throws IOException
	 */
	@Override
	public void delete (String id) throws IOException {
		store.delete(id);
	}
	
	/**
	 * Liefert eine Liste von BannerDefinitionen, die die Kriterien des Request erfüllen
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@Override
	public List<ContentDefinition> search (ContentRequest request) throws IOException {
		List<ContentDefinition> result = this.store.search(request);
		
		/*
		 * Zu letzt wird das Ergebnis gefiltert
		 * 
		 * Grund dafür ist, dass wenn ein Banner nur in einem 5 KM Radius um
		 * eine Stadt angezeigt werden soll, das aktuell noch nicht über eine
		 * Query gemacht werden kann. Für dieses Dinge können Filter verwendet
		 * werden
		 */
		result = ConditionHelper.getInstance().processFilter(request, result, this);

		return result;
	}
	
	/**
	 * Liefert ein Banner für eine ID
	 * @param id Die ID des Banners
	 * @return
	 */
	@Override
	public Optional<ContentDefinition> get (String id) {
		return this.store.get(id);
	}
	
	/**
	 * liefert die Anzahl der Banner in der Datenbank
	 * @return
	 */
	@Override
	public int size () throws IOException {		
		return this.store.size();
	}
}

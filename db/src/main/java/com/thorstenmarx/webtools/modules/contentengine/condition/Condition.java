/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition;



import java.io.Serializable;

import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Bedingung die ein Banner erfüllen kann oder muss
 * 
 * Eine Condition erweitert zum einen das Dokument, das im Index gespeichert wird
 * zum anderen wird das Query zum suchen passender Banner erweitert um die entsprechende 
 * Condition zu erfüllen 
 * 
 * @author thmarx
 *
 */
public interface Condition<D, Q> extends Serializable {
	/**
	 * Aufbereitung des Queries
	 * 
	 * @param request
	 * @param mainQuery
	 */
	public void addQuery (ContentRequest request, Q mainQuery);
	/**
	 * Erweitert das Dokument um die für diese Bedingung benötigten Felder
	 * 
	 * @param adDoc
	 * @param bannerDefinition
	 */
	public void addFields (D adDoc, ContentDefinition bannerDefinition);
}

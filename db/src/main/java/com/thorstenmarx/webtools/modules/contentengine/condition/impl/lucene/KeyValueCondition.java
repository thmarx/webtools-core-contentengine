/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.AbstractCondition;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.KeyValue;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.KeyValueConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;


/**
 * Keyword-Bedingung
 * 
 * Die Klasse erweitert das Document und das Query
 * 
 * @author tmarx
 *
 */
public class KeyValueCondition extends AbstractCondition implements Condition<Document, BooleanQuery.Builder> {

	
	public KeyValueCondition (ContentDBImpl addb) {
		super(addb);
	}
	
	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.keyValues() == null || request.keyValues().isEmpty()) {
			return;
		}
		
		BooleanQuery.Builder query = new BooleanQuery.Builder();
		
		// keyvalues einfügen
//		request.keyValues().keySet().forEach((k) -> {
//			query.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_KEYVALUE + "_" + k , request.keyValues().get(k))), Occur.MUST);
//		});
		request.keyValues().forEach((key, value) -> {
			query.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_KEYVALUE + "_" + key , value)), Occur.MUST);
		});
		
		
		mainQuery.add(query.build(), Occur.MUST);
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		
		KeyValueConditionDefinition kdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.KEYVALUE)) {
			kdef = (KeyValueConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.KEYVALUE);
		}
		

		if (kdef != null && !kdef.getKeyValues().isEmpty()) {
			// keyvalues im Dokument speichern
			List<KeyValue> kws = kdef.getKeyValues();
			kws.forEach((k) -> {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_KEYVALUE + "_" + k.key, k.value, Field.Store.NO));
			});
		} 
	}

}

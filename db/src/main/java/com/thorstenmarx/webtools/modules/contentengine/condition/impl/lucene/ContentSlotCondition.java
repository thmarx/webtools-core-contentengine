/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentSlot;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.ContentSlotConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import org.apache.lucene.util.QueryBuilder;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Beschränkung der Anzeige eines Banners oder Products auf einen bestimmten Anzeige Platz (AdSlot)
 * 
 * AdSlots machen für die Definition von Produkten den meisten Sinn
 * 
 * @author thmarx
 *
 */
public class ContentSlotCondition implements Condition<Document, BooleanQuery.Builder> {

	private static final long serialVersionUID = 436792306193069995L;	
	
	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		BooleanQuery.Builder query = new BooleanQuery.Builder();
		
		BooleanQuery.Builder temp = new BooleanQuery.Builder();
		
		/*
		 * wir ein slot im Request übergeben, werden alle Banner geladen, die 
		 * für diesen Slot angezeigt werden sollten
		 * und alle Banner, für die es keine Einschränkung gibt
		 */
		if (request.adSlot() != null) {
			// AdSlot einfügen
			temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_SLOT, request.adSlot())), Occur.SHOULD);
			
			query.add(temp.build(), Occur.MUST);
			mainQuery.add(query.build(), Occur.MUST);
		}
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		
		ContentSlotConditionDefinition sdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.ADSLOT)) {
			sdef = (ContentSlotConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.ADSLOT);
		}
		
		if (sdef != null && !sdef.getSlots().isEmpty()) {
			// AdSlots im Dokument speichern
			List<ContentSlot> slots = sdef.getSlots();
			slots.stream().forEach((slot) -> {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_SLOT, slot.toString(), Field.Store.NO));
			});
		} else {
			/*
			 * Banner, die keine Einschräkung auf einen spezielle AdSlot haben
			 */
//			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_SLOT, ContentEngineConstants.CONTENT_SLOT_ALL, Field.Store.NO));
		}
	}

}

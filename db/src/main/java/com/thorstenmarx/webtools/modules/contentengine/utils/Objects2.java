package com.thorstenmarx.webtools.modules.contentengine.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author marx
 */
public class Objects2 {

	public static byte[] toByteArray(Object value) throws IOException {
		try (
				final ByteArrayOutputStream bos = new ByteArrayOutputStream();
				final ObjectOutputStream oos = new ObjectOutputStream(bos)) {
			oos.writeObject(value);
			oos.flush();
			return bos.toByteArray();
		}
	}

	public static <T> T toObject(byte[] bytes, Class<T> clazz) throws IOException, ClassNotFoundException {
		Object obj = null;

		try (
				final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
				final ObjectInputStream ois = new ObjectInputStream(bis)) {
			return (T) ois.readObject();
		}
	}
}

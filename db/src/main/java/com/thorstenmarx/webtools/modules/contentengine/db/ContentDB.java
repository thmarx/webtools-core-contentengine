/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db;

import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author marx
 */
public interface ContentDB {

	/**
	 * Schreibt ein Banner in den Index und in die Datenbank
	 *
	 * @param banner
	 * @throws IOException
	 */
	void add(ContentDefinition banner) throws IOException;

	void clear() throws IOException;

	/**
	 * Löscht ein Banner aus dem Index und der Datenbank
	 * @param id
	 * @throws IOException
	 */
	void delete(String id) throws IOException;

	/**
	 * Liefert ein Banner für eine ID
	 * @param id Die ID des Banners
	 * @return
	 */
	Optional<ContentDefinition> get(String id);

	/**
	 * Liefert eine Liste von BannerDefinitionen, die die Kriterien des Request erfüllen
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 */
	List<ContentDefinition> search(ContentRequest request) throws IOException;

	/**
	 * liefert die Anzahl der Banner in der Datenbank
	 * @return
	 */
	int size() throws IOException;
	
}

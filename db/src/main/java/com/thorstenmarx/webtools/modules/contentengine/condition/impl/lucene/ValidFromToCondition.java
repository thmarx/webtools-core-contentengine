/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.condition.impl.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.util.BytesRef;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineConstants;
import com.thorstenmarx.webtools.modules.contentengine.condition.Condition;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.ValidFromToConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.ValidFromToConditionDefinition.Period;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

/**
 * Bedingung bzgl. des Zeitraums in dem das Banner angezeigt werden soll
 * z.B. zwischen 23 und 4 Uhr 
 * 
 * 04.07.2011:
 * Nach der Anpassung können im Banner nun Perioden angegeben werden, wann ein Banner
 * angezeigt werden soll 
 * zwischen 8 und 10 Uhr und zwischen 12 und 14 Uhr 
 * 
 * @author tmarx
 *
 */
public class ValidFromToCondition implements Condition<Document, BooleanQuery.Builder> {

	@Override
	public void addQuery(ContentRequest request, BooleanQuery.Builder mainQuery) {
		if (request.time() != null && request.date() != null) { 
			
			// build the correct format
			String currentDate = request.date() + request.time();
			
			BooleanQuery.Builder query = new BooleanQuery.Builder();
			
			BooleanQuery.Builder temp = new BooleanQuery.Builder();
			TermRangeQuery tQuery = new TermRangeQuery(ContentEngineConstants.CONTENT_VALID_FROM, null, new BytesRef(currentDate), true, true);
			temp.add(tQuery, Occur.SHOULD);
			temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_VALID_FROM, ContentEngineConstants.CONTENT_VALID_ALL)), Occur.SHOULD);
			
			query.add(temp.build(), Occur.MUST);
			
			temp = new BooleanQuery.Builder();
			tQuery = new TermRangeQuery(ContentEngineConstants.CONTENT_VALID_TO, new BytesRef(currentDate), null, true, true);
			temp.add(tQuery, Occur.SHOULD);
			temp.add(new TermQuery(new Term(ContentEngineConstants.CONTENT_VALID_TO, ContentEngineConstants.CONTENT_VALID_ALL)), Occur.SHOULD);
			
			query.add(temp.build(), Occur.MUST);
			
			mainQuery.add(query.build(), Occur.MUST);
			
		}
	}

	@Override
	public void addFields(Document bannerDoc, ContentDefinition bannerDefinition) {
		ValidFromToConditionDefinition tdef = null;
		if (bannerDefinition.hasConditionDefinition(ConditionDefinitions.VALIDFROMTO)) {
			tdef = (ValidFromToConditionDefinition) bannerDefinition.getConditionDefinition(ConditionDefinitions.VALIDFROMTO);
		}
		
		if (tdef != null && tdef.getPeriod() != null) {
			Period p = tdef.getPeriod();
			if (p.getFrom() != null) {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_VALID_FROM, p.getFrom(), Field.Store.NO));
			} else {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_VALID_FROM, ContentEngineConstants.CONTENT_VALID_ALL, Field.Store.NO));
			}
			
			if (p.getTo() != null) {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_VALID_TO, p.getTo(), Field.Store.NO));
			} else {
				bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_VALID_TO, ContentEngineConstants.CONTENT_VALID_ALL, Field.Store.NO));
			} 
		} else {
			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_VALID_FROM, ContentEngineConstants.CONTENT_VALID_ALL, Field.Store.NO));
			bannerDoc.add(new StringField(ContentEngineConstants.CONTENT_VALID_TO, ContentEngineConstants.CONTENT_VALID_ALL, Field.Store.NO));
		}
	}

}

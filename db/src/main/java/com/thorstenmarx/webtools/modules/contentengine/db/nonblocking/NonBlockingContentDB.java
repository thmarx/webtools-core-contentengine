/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.db.nonblocking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;

public class NonBlockingContentDB extends ContentDBImpl {

	public NonBlockingContentDB(ContentEngineManager manager) {
		super(manager);
	}

	
	public void search(final ContentRequest request, final ReturnFunction<List<ContentDefinition>> returnFunction) throws IOException {
		if (manager.getExecutorService() == null) {
			manager.getExecutorService().execute(() -> {
				try {
					returnFunction.handle(search(request));
				} catch (IOException e) {
					LOGGER.warn("", e);
					returnFunction.handle(new ArrayList<>());
				}
			});
		} else {
			returnFunction.handle(super.search(request));
		}
	}
}

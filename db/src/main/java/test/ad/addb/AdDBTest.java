/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test.ad.addb;


import com.thorstenmarx.webtools.modules.contentengine.ContentEngineContext;
import java.io.IOException;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;

public class AdDBTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		ContentEngineContext context = new ContentEngineContext();
		context.getConfiguration().put(LocalContentStore.CONFIG_DATADIR, "/www/tmp/data/");
		ContentEngineManager manager = ContentEngineManager.builder().mode(Mode.LOCAL).context(context).build();
		manager.getAdDB().open();
		
		System.out.println(manager.getAdDB().size());
		
		ContentDefinition ib = ContentDefinitionImpl.forType("image");
		ib.setId("1");
		manager.getAdDB().delete(ib.getId());
		manager.getAdDB().add(ib);
		
		ib = ContentDefinitionImpl.forType("image");
		ib.setId("2");
		manager.getAdDB().delete(ib.getId());
		manager.getAdDB().add(ib);
		
		manager.getAdDB().reopen();
		
		ContentRequest request = new ContentRequest();
		request.types().add("image");
		
		List<ContentDefinition> result = manager.getAdDB().search(request);
		
		System.out.println(result.size());
		
		
		manager.getAdDB().close();
	}

}

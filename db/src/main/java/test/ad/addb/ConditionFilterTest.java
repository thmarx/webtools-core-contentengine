/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test.ad.addb;

import java.util.ArrayList;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.CountryConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.DistanceConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.model.Country;
import com.thorstenmarx.webtools.modules.contentengine.utils.geo.GeoLocation;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;

public class ConditionFilterTest {
	public ConditionFilterTest () {
		
	}
	
	public void doGeoLocationTest () throws Exception {
		System.out.println("geo test");
		ContentEngineManager manager = ContentEngineManager.builder().build();
		
		manager.getAdDB().open();
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		
		CountryConditionDefinition cdef = new CountryConditionDefinition();
		cdef.addCountry(new Country("DE"));
		b.addConditionDefinition(ConditionDefinitions.COUNTRY, cdef);
		
		GeoLocation gl = new GeoLocation(51.4844, 7.2188);
		DistanceConditionDefinition dcdef = new DistanceConditionDefinition();
		dcdef.setGeoLocation(gl);
		dcdef.setGeoRadius(5);
		b.addConditionDefinition(ConditionDefinitions.DISTANCE, dcdef);
		
		manager.getAdDB().add(b);
		
		manager.getAdDB().reopen();
		
		ContentRequest request = new ContentRequest();
		List<String> types = new ArrayList<>();
		types.add("image");
		request.types(types);
		
		gl = new GeoLocation(51.4863, 7.180);
		request.geoLocation(gl);
		
		List<ContentDefinition> result = manager.getAdDB().search(request);
		System.out.println(result.size());
		
		
		gl = new GeoLocation(51.4857, 7.0958);
		request.geoLocation(gl);
		result = manager.getAdDB().search(request);
		System.out.println(result.size());
		
		
		manager.getAdDB().close();
	}
	
	public static void main (String [] args) throws Exception {
		ConditionFilterTest st = new ConditionFilterTest();
		
		st.doGeoLocationTest();
	}
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import com.google.common.io.Files;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

@Test(invocationCount = 10)
public abstract class AdDBTestCase  {

	protected static ContentDBImpl db = null;
	protected static ContentEngineManager manager = null;

	private static File tempDir;

	@BeforeClass
	public static void startup() {
		System.out.println("create temp folder");
		tempDir = Files.createTempDir();
	}

	@AfterClass
	public static void shutdown() throws Exception {
		System.out.println("delete temp folder");
		deleteDirectory(tempDir);
	}

	@BeforeMethod
	public void setUp() throws IOException {
		manager = ContentEngineManager.builder().build();

		manager.getContext().mode = Mode.LOCAL;

		manager.getContext().getConfiguration().put(LocalContentStore.CONFIG_DATADIR, tempDir.getAbsolutePath());

		db = manager.getAdDB();
		db.open();

		System.out.println("opened");
	}

	@AfterMethod
	public void tearDown() throws IOException {
		db.clear();
		db.close();
		System.out.println("cleared & closed");
	}

	static public boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					deleteDirectory(file);
				} else {
					file.delete();
				}
			}
		}
		return (path.delete());
	}
}

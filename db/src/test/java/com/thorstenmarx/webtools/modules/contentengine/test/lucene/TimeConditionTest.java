/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import java.util.ArrayList;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.TimeConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class TimeConditionTest extends AdDBTestCase {
	
	@Test(enabled = false)
	public void testTimeCondition () throws Exception {
		System.out.println("TimeCondition");
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		TimeConditionDefinition sdef = new TimeConditionDefinition();
		sdef.addPeriod("0800", "1000");
		sdef.addPeriod("1800", "2000");
		b.addConditionDefinition(ConditionDefinitions.TIME, sdef);
		db.add(b);
		
		db.reopen();
		
		ContentRequest request = new ContentRequest();
		request.types().add("image");
		
		request.time("0730");
		List<ContentDefinition> result = db.search(request);
		assertEquals(0, result.size());
		
		request.time("0800");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.time("0900");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.time("1000");
		result = db.search(request);
		assertEquals(1, result.size());
		
		
		request.time("1001");
		result = db.search(request);
		assertEquals(0, result.size());
		
		request.time("1759");
		result = db.search(request);
		assertEquals(0, result.size());
		
		request.time("1800");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.time("1900");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.time("2000");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.time("2001");
		result = db.search(request);
		assertEquals(0, result.size());
		
	}
}

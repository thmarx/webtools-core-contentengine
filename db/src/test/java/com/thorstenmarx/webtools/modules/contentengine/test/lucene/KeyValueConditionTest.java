/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;



import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.KeyValue;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.KeyValueConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class KeyValueConditionTest extends AdDBTestCase {
	
	@Test
	public void test1_KeyValueCondition () throws Exception {
		System.out.println("test1_KeyValueCondition");
		
		
		manager.getContext().validKeys.clear();
		manager.getContext().validKeys.add("browser");
		
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		KeyValueConditionDefinition sdef = new KeyValueConditionDefinition();
		sdef.getKeyValues().add(new KeyValue("browser", "firefox"));
		sdef.getKeyValues().add(new KeyValue("browser", "chrome"));
		b.addConditionDefinition(ConditionDefinitions.KEYVALUE, sdef);
		
		db.add(b);
		
		b = ContentDefinitionImpl.forType("image");
		b.setId("2");
		sdef = new KeyValueConditionDefinition();
		sdef.getKeyValues().add(new KeyValue("browser", "firefox"));
		sdef.getKeyValues().add(new KeyValue("browser", "ie"));
		b.addConditionDefinition(ConditionDefinitions.KEYVALUE, sdef);
		db.add(b);
		
		db.reopen();
		
		List<String> types = new ArrayList<>();
		types.add("image");
		ContentRequest request = new ContentRequest();
		request.types(types);
		
		request.addKeyValue("browser", "opera");
		
		List<ContentDefinition> result = db.search(request);
		assertTrue(result.isEmpty());
		
		request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "firefox");
		result = db.search(request);
		assertEquals(2, result.size());
		
		request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "chrome");
		
		result = db.search(request);
		assertEquals(1, result.size());
		assertTrue(result.get(0).getId().equals("1"));
		
		request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "ie");
		
		result = db.search(request);
		assertEquals(1, result.size());
		assertTrue(result.get(0).getId().equals("2"));
		
	}
	

	@Test
	public void test2_KeyValueCondition () throws Exception {
		System.out.println("test2_KeyValueCondition");
		
		manager.getContext().validKeys.clear();
		manager.getContext().validKeys.add("browser");
		manager.getContext().validKeys.add("os");
		
		db.open();
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		KeyValueConditionDefinition sdef = new KeyValueConditionDefinition();
		sdef.getKeyValues().add(new KeyValue("browser", "firefox"));
		sdef.getKeyValues().add(new KeyValue("browser", "chrome"));
		sdef.getKeyValues().add(new KeyValue("os", "osx"));
		sdef.getKeyValues().add(new KeyValue("os", "linux"));
		b.addConditionDefinition(ConditionDefinitions.KEYVALUE, sdef);
		db.add(b);
		
		b = ContentDefinitionImpl.forType("image");
		b.setId("2");
		sdef = new KeyValueConditionDefinition();
		sdef.getKeyValues().add(new KeyValue("browser", "firefox"));
		sdef.getKeyValues().add(new KeyValue("browser", "ie"));
		sdef.getKeyValues().add(new KeyValue("os", "windows"));
		b.addConditionDefinition(ConditionDefinitions.KEYVALUE, sdef);
		db.add(b);
		
		b = ContentDefinitionImpl.forType("image");
		b.setId("3");
		db.add(b);
		
		db.reopen();
		
		List<String> types = new ArrayList<>();
		types.add("image");
		
		ContentRequest request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "opera");
		request.addKeyValue("os", "linux");
		
		List<ContentDefinition> result = db.search(request);
		assertEquals(result.size(), 0);
		
		request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "firefox");
		request.addKeyValue("os", "osx");
		result = db.search(request);
		assertEquals(1, result.size());
		
		
		request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "chrome");
		request.addKeyValue("os", "osx");
		
		result = db.search(request);
		assertEquals(1, result.size());
		
		request = new ContentRequest();
		request.types(types);
		request.addKeyValue("browser", "ie");
		request.addKeyValue("os", "windows");
		
		result = db.search(request);
		assertEquals(1, result.size());
		
	}
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;


import java.io.IOException;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;
import java.util.Optional;


public class StoredAdDBTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		ContentEngineManager manager = ContentEngineManager.builder().build();
		manager.getContext().mode = Mode.LOCAL;
		manager.getContext().getConfiguration().put(LocalContentStore.CONFIG_DATADIR, "target/content");
		manager.getAdDB().open();
		
		manager.getAdDB().clear();
		
		for (int i = 0; i < 10000; i++) {
			ContentDefinition def = ContentDefinitionImpl.forType("image");
			def.setId(String.valueOf(i+1));
			manager.getAdDB().add(def);
			
		}
		
		manager.getAdDB().reopen();
		
		ContentRequest request = new ContentRequest();
		request.types().add("image");
		
		List<ContentDefinition> result = manager.getAdDB().search(request);
		
		System.out.println(result.size());
		
		for (int i = 0; i < 500; i++) {
			Optional<ContentDefinition> def = manager.getAdDB().get(String.valueOf(i+1));
			System.out.println(def.isPresent());
		}
		
		manager.getAdDB().close();
	}

}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.LanguageConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class LanguageConditionTest extends AdDBTestCase {
	
	@Test
	public void testLanguageCondition () throws Exception {
		System.out.println("LanguageCondition");
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		
		LanguageConditionDefinition sdef = new LanguageConditionDefinition();
		sdef.getLanguages().add("de");
		sdef.getLanguages().add("en");
		b.addConditionDefinition(ConditionDefinitions.LANGUAGE, sdef);
		db.add(b);
		
		db.reopen();
		
		ContentRequest request = new ContentRequest();
		List<String> types = new ArrayList<>();
		types.add("image");
		request.types(types);
		request.locale(new Locale("nl"));
		
		List<ContentDefinition> result = db.search(request);
		assertTrue(result.isEmpty());
		
		request.locale(new Locale("de"));
		
		result = db.search(request);
		assertEquals(result.size(), 1);
		
		
	}
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import java.util.ArrayList;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.DayConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.enums.Day;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class DayConditionTest extends AdDBTestCase {
	
	@Test
	public void testDayCondition () throws Exception {
		System.out.println("DayCondition");
		
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		
		DayConditionDefinition sdef = new DayConditionDefinition();
		sdef.addDay(Day.Monday);
		sdef.addDay(Day.Wednesday);
		b.addConditionDefinition(ConditionDefinitions.DAY, sdef);
		db.add(b);
		
		b = ContentDefinitionImpl.forType("image");
		b.setId("2");
		sdef = new DayConditionDefinition();
		sdef.addDay(Day.Tuesday);
		b.addConditionDefinition(ConditionDefinitions.DAY, sdef);
		db.add(b);
		
		db.reopen();
		System.out.println(db.size());
		
		ContentRequest request = new ContentRequest();
		List<String> types = new ArrayList<>();
		types.add("image");
		request.types(types);
		request.day(Day.Tuesday);
		
		List<ContentDefinition> result = db.search(request);
		assertEquals(1, result.size());
		
		request.day(Day.Monday);
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.day(Day.Wednesday);
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.day(Day.Sunday);
		result = db.search(request);
		assertEquals(0, result.size());
		
	}
}

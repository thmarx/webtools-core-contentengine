/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;


import static org.testng.Assert.*;
import org.testng.annotations.Test;


import com.thorstenmarx.webtools.modules.contentengine.definition.condition.CountryConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.model.Country;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;



public class RealtimeTest extends AdDBTestCase {
	
	@Test
	public void testRealtime () throws Exception {
		System.out.println("Realtime");
		

		long before = System.currentTimeMillis();
		for (int i = 0; i < 500; i++) {
			ContentDefinition b = ContentDefinitionImpl.forType("image");
			b.setId("" + (i+1));
			CountryConditionDefinition sdef = new CountryConditionDefinition();
			sdef.addCountry(new Country("DE"));
			b.addConditionDefinition(ConditionDefinitions.COUNTRY, sdef);
			db.add(b);
		}
		
		long after = System.currentTimeMillis();
		System.out.println((after - before) + "ms");
		
		
		assertEquals(0, db.size());
		
		before = System.currentTimeMillis();
		
		db.reopen();
		
		after = System.currentTimeMillis();
		
		System.out.println((after - before) + "ms");
		
		
		assertEquals(db.size(), 500);
		
		db.delete("5");
		
		assertEquals(db.size(), 500);
		
		db.reopen();
		
		assertEquals(db.size(), 499);
		
		
	}
}

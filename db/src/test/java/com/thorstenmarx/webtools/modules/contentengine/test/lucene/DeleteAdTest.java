/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;


import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;



public class DeleteAdTest extends AdDBTestCase {
	
	@Test
	public void testDelete () throws Exception {
		System.out.println("delete");
		
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		db.add(b);
		
		db.reopen();
		
		assertThat(db.size()).isEqualTo(1);
		
		db.delete("1");
		
		db.reopen();
		
		assertThat(db.size()).isEqualTo(0);
	}
}

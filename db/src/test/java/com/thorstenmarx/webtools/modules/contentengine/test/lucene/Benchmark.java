/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;


import java.io.IOException;

import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;



public class Benchmark  {
	
	public static void main(String[] args) throws IOException {
		
		ContentEngineManager manager = ContentEngineManager.builder().build();
		manager.getContext().mode = Mode.LOCAL;
		manager.getContext().getConfiguration().put(LocalContentStore.CONFIG_DATADIR, "D:/www/apps/adserver/temp/");
		manager.getAdDB().open();
		
		manager.getAdDB().clear();
		
		System.out.println("adding ads ");
		System.out.println("-----------");
		int count = 10000;
		long before = System.currentTimeMillis();
		for (int i = 1; i <= count; i++) {
			ContentDefinition ib = ContentDefinitionImpl.forType("image");
			ib.setId(""+i);
			manager.getAdDB().add(ib);
		}
		long after = System.currentTimeMillis();
		
		System.out.println(count +" ads took " + (after - before));
		System.out.println("per ad " + ((after - before) / (float)count));
		
		System.out.println("reopen db");
		System.out.println("---------");
		before = System.currentTimeMillis();
		manager.getAdDB().reopen();
		
		after = System.currentTimeMillis();
		
		System.out.println("reopen took " + (after - before));
		
		manager.getAdDB().close();
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class JSTimeTest {

	@Test
	public void testJSDateTime() {
		ZonedDateTime zdt = ZonedDateTime.parse("Tue, 06 Aug 2019 12:11:42 GMT", DateTimeFormatter.RFC_1123_DATE_TIME);
		System.out.println(zdt.getDayOfWeek().getValue());

		zdt = ZonedDateTime.parse("Tue, 06 Aug 2019 12:20:13 GMT", DateTimeFormatter.RFC_1123_DATE_TIME);
		System.out.println(zdt.getDayOfWeek().getValue());
		System.out.println(zdt.getHour());
		System.out.println(zdt.toLocalDateTime().getHour());
	}

	@Test
	public void testZoneOfsettime() {
		ZoneOffset zoneOffSet = ZoneOffset.ofTotalSeconds(-120 * 60 * -1);
		OffsetDateTime date = OffsetDateTime.now(zoneOffSet);
		System.out.println(date.getHour());
	}

	@Test
	public void testFromMilli() {
		// "Tue Aug 06 2019 14:36:27 GMT+0200 (Mitteleuropäische Sommerzeit)"
		long millisconds = 1565094987439L;
		LocalDateTime date = Instant.ofEpochMilli(millisconds).atZone(ZoneId.systemDefault()).toLocalDateTime();
		System.out.println(date.toString());
	}
}

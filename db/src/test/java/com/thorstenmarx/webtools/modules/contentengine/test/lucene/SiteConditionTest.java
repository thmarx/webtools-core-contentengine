/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.SiteConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class SiteConditionTest extends AdDBTestCase {
	
	@Test
	public void testSiteCondition () throws Exception {
		System.out.println("SiteCondition");
		
		ContentDefinition b = getAd(new String [] {"10"}, "1");
		db.add(b);
		
		b = getAd(new String [] {"11"}, "2");
		db.add(b);
		
		db.reopen();
		
		System.out.println(db.size());
		
		ContentRequest request = new ContentRequest();
		List<String> types = new ArrayList<String>();
		types.add("image");
		request.types(types);
		request.site("1");
		
		List<ContentDefinition> result = db.search(request);
		assertTrue(result.isEmpty());
		
		request.site("10");
		result = db.search(request);
		assertEquals(1, result.size());
		assertTrue(result.get(0).getId().equals("1"));
		
		
		request.site("11");
		result = db.search(request);
		assertEquals(1, result.size());
		assertTrue(result.get(0).getId().equals("2"));

		request.site("12");
		result = db.search(request);
		assertEquals(0, result.size());
		
		b = getAd(new String [] {"12"}, "3");
		db.add(b);
		
		
		System.out.println(db.size());
		db.reopen();
		System.out.println(db.size());
		
		request = new ContentRequest();
		request.types(types);
		request.site("12");
		result = db.search(request);
		assertEquals(1, result.size());
		assertTrue(result.get(0).getId().equals("3"));
		
		
	}
	
	private static ContentDefinition getAd (String []sites, String id) {
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId(id);
		SiteConditionDefinition sdef = new SiteConditionDefinition();
		for (String site : sites) {
			sdef.addSite(site);
		}
		b.addConditionDefinition(ConditionDefinitions.SITE, sdef);
		
		
		
		return b;
	}
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;



import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.ValidFromToConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class ValidFromToConditionTest extends AdDBTestCase {
	
	@Test
	public void testTimeCondition () throws Exception {
		System.out.println("TimeCondition");
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		ValidFromToConditionDefinition sdef = new ValidFromToConditionDefinition();
		sdef.setPeriod("201308010800", "201308021300");
		b.addConditionDefinition(ConditionDefinitions.VALIDFROMTO, sdef);
		db.add(b);
		
		db.reopen();
		
		ContentRequest request = new ContentRequest();
		request.types().add("image");
		
		request.date("20130801");
		request.time("0730");
		List<ContentDefinition> result = db.search(request);
		assertEquals(0, result.size());
		
		request.date("20130801");
		request.time("0800");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.date("20130802");
		request.time("1000");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.date("20130802");
		request.time("1301");
		result = db.search(request);
		assertEquals(0, result.size());
		
	}
}

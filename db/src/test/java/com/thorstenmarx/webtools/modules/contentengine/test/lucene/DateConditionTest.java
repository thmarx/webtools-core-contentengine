/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.DateConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class DateConditionTest extends AdDBTestCase {
	
	@Test(invocationCount = 1, enabled = false)
	public void testDateCondition () throws Exception {
		System.out.println("CountryCondition");
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		DateConditionDefinition sdef = new DateConditionDefinition();
		sdef.addPeriod("20110215", "20110220");
		sdef.addPeriod("20110315", "20110320");
		b.addConditionDefinition(ConditionDefinitions.DATE, sdef);
		db.add(b);
		
		db.reopen();
		
		ContentRequest request = new ContentRequest();
		List<String> types = new ArrayList<>();
		types.add("image");
		request.types(types);
		
		request.date("20110214");
		List<ContentDefinition> result = db.search(request);
		assertEquals(0, result.size());
		
		request.date("20110215");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.date("20110220");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.date("20110221");
		result = db.search(request);
		assertEquals(0, result.size());
		
		request.date("20110314");
		result = db.search(request);
		assertEquals(0, result.size());
		
		request.date("20110315");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.date("20110320");
		result = db.search(request);
		assertEquals(1, result.size());
		
		request.date("20110321");
		result = db.search(request);
		assertEquals(0, result.size());
	}
}

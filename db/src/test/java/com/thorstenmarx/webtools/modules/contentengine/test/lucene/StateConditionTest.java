/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 2015 MarxMedien (http://marx-medien.de)
 */
package com.thorstenmarx.webtools.modules.contentengine.test.lucene;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;



import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.StateConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.model.State;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;


public class StateConditionTest extends AdDBTestCase {
	
	@Test
	public void testStateCondition () throws Exception {
		System.out.println("StateCondition");
		
		ContentDefinition b = ContentDefinitionImpl.forType("image");
		b.setId("1");
		
		StateConditionDefinition sdef = new StateConditionDefinition();
		sdef.addState(new State("BB"));
		b.addConditionDefinition(ConditionDefinitions.STATE, sdef);
		db.add(b);
		
		db.reopen();
		
		ContentRequest request = new ContentRequest();
		List<String> types = new ArrayList<>();
		types.add("image");
		request.types(types);
		request.state(new State("BE"));
		
		List<ContentDefinition> result = db.search(request);
		assertTrue(result.isEmpty());
		
		request.state(new State("BB"));
		
		result = db.search(request);
		assertEquals(result.size(), 1);
		
		
	}
}

/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.thorstenmarx.webtools.modules.contentengine.endpoints;


import com.thorstenmarx.webtools.modules.contentengine.endpoints.manager.ContentEndpoint;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.api.entities.Entities;
import com.thorstenmarx.webtools.api.entities.Store;
import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.Unirest;
import io.github.openunirest.http.exceptions.UnirestException;
import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.beans.Experience;
import com.thorstenmarx.webtools.modules.contentengine.beans.Experiences;
import com.thorstenmarx.webtools.modules.contentengine.beans.Variant;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTestNg;
import org.glassfish.jersey.test.TestProperties;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class ContentEndPointTest extends JerseyTestNg.ContainerPerClassTest {

	Entities entities;
	Store<Experience> store;

	protected static ContentDBImpl db = null;
	protected static ContentEngineManager manager = null;


	@Override
	public void tearDown() throws Exception {
		super.tearDown(); //To change body of generated methods, choose Tools | Templates.
		db.clear();
		db.close();
	}
	
	@Override
	protected Application configure() {
		
		enable(TestProperties.LOG_TRAFFIC);
		enable(TestProperties.DUMP_ENTITY);
		
		entities = new MockEntities();
		store = entities.store(Experience.class);
		
		
		manager = ContentEngineManager.builder().build();

		manager.getContext().mode = Mode.LOCAL;

		manager.getContext().getConfiguration().put(LocalContentStore.CONFIG_DATADIR, "build/content-" + System.currentTimeMillis());

		db = manager.getAdDB();
		try {
			db.open();
		} catch (IOException ex) {
			Logger.getLogger(ContentEndPointTest.class.getName()).log(Level.SEVERE, null, ex);
			throw new RuntimeException(ex);
		}
		
		ResourceConfig resourceConfig = new ResourceConfig();
		
		resourceConfig.registerInstances(new ContentEndpoint(store, db));
		
		return resourceConfig;
	}

	@Test
	public void test() {
		String experiences = target("/info").request().get(String.class);
		System.out.println(experiences);
	}
	
	@Test(expectedExceptions = WebApplicationException.class)
	public void add_no_type() {
		Entity<String> experience = Entity.entity("{}", MediaType.APPLICATION_JSON);
		String id = target("/").request().post(experience, String.class);
	}
	@Test()
	public void add_id_returned() {
//		EasyMock.replay(store);
		Experience exp = new Experience();
		exp.setType("experience");
		Entity<Experience> experience = Entity.entity(exp, MediaType.APPLICATION_JSON);
		String result = target("/").request(MediaType.APPLICATION_JSON).post(experience, String.class);
		
		JSONObject obj = JSONObject.parseObject(result);
		Assertions.assertThat(obj.getString("id")).isNotBlank();
		
	}
	@Test()
	public void add_complete() {
		Experience exp = new Experience();
		exp.setType("experience");
		exp.setFrom(new Date());
		exp.setTo(new Date());
		exp.setName("The name");
		exp.setPage("apage");
		Variant v1 = new Variant();
		v1.setContent("<h1>ich bin content</h1>");
		v1.setEditor("textblock");
		v1.setName("ich bin eine variante");
		v1.setSelector("#header");
		exp.setVariants(Arrays.asList(v1));
		
		Entity<Experience> experience = Entity.entity(exp, MediaType.APPLICATION_JSON);
		String result = target("/").request().post(experience, String.class);
		
		JSONObject obj = JSONObject.parseObject(result);
		Assertions.assertThat(obj.getString("id")).isNotBlank();
		
	}
	
	@Test()
	public void add_list () {
		
		
		Experience exp = new Experience();
		exp.setType("experience");
		exp.setFrom(new Date());
		exp.setTo(new Date());
		exp.setName("The name");
		exp.setPage("apage");
		Variant v1 = new Variant();
		v1.setContent("<h1>ich bin content</h1>");
		v1.setEditor("textblock");
		v1.setName("ich bin eine variante");
		v1.setSelector("#header");
		exp.setVariants(Arrays.asList(v1));
		
		Experiences experiences = new Experiences();
		experiences.setPage("apage");
		experiences.setExperiences(Arrays.asList(exp, exp));
		
		Entity<Experiences> experience = Entity.entity(
				experiences
				, MediaType.APPLICATION_JSON
		);
		String response = target("/list").request().post(experience, String.class);
		
		System.out.println(response);
	}
	
	@Test
	public void plain_json () throws UnirestException {
		JSONObject json = new JSONObject();
		json.put("type", "experience");
		json.put("from", "01/01/2018");
		json.put("to", "01/31/2018");
		json.put("days", Arrays.asList("mon", "tues"));
		
		System.out.println(getBaseUri().toString());
		final HttpResponse<String> result = Unirest.post(getBaseUri().toString())
				.header("accept", "application/json")
				.header("Content-Type", "application/json; charset=utf8")
				.body(json.toString())
				.asString();
		System.out.println(result.getStatus());
		System.out.println(result.getStatusText());
		System.out.println(result.getBody());
	}
	
	@Test()
	public void add_list_and_get () {
		
		
		Experience exp = new Experience();
		exp.setType("experience");
		exp.setFrom(new Date());
		exp.setTo(new Date());
		exp.setName("The name");
		exp.setPage("apage22");
		Variant v1 = new Variant();
		v1.setContent("<h1>ich bin content</h1>");
		v1.setEditor("textblock");
		v1.setName("ich bin eine variante");
		v1.setSelector("#header");
		exp.setVariants(Arrays.asList(v1));
		
		Experiences experiences = new Experiences();
		experiences.setPage("apage22");
		experiences.setExperiences(Arrays.asList(exp, exp));
		
		Entity<Experiences> experience = Entity.entity(
				experiences
				, MediaType.APPLICATION_JSON
		);
		String response = target("/list").request().post(experience, String.class);
		
		response = target("list").queryParam("page", "apage22").request().get(String.class);
		
		System.out.println(response);
	}
}

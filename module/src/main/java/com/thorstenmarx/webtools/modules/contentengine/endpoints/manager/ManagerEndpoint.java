/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.endpoints.manager;

import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.webtools.api.entities.Entities;
import com.thorstenmarx.webtools.api.entities.Store;
import com.thorstenmarx.webtools.api.extensions.ManagerRestResourceExtension;
import com.thorstenmarx.webtools.modules.contentengine.beans.Experience;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDB;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.module.ContentEngineModuleLifeCycle;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author marx
 */
@Path("/")
@Extension(ManagerRestResourceExtension.class)
public class ManagerEndpoint extends ManagerRestResourceExtension {

	@Inject
	private Entities entities;
	
	private Store<Experience> store;
	
	private ContentEndpoint contentEndpoint;
	
	@Override
	public void init() {
		store = entities.store(Experience.class);
		this.contentEndpoint = new ContentEndpoint(store, ContentEngineModuleLifeCycle.DB);
	}
	
	@Path("content")
	public ContentEndpoint content () {
		return contentEndpoint;
		// Das Funktioniert nicht, da wir nicht überall mit Interfaces arbeiten
//		return new ContentEndpoint(store, getContext().serviceRegistry().single(ContentDB.class).get());
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/info")
	public String info () {
		return "info";
	}
	
}

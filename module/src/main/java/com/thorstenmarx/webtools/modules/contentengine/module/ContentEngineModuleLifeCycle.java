/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.module;


import com.thorstenmarx.modules.api.ModuleLifeCycleExtension;
import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.webtools.modules.contentengine.ContentEngineManager;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDB;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.db.store.impl.local.LocalContentStore;
import com.thorstenmarx.webtools.modules.contentengine.enums.Mode;
import java.io.File;
import java.io.IOException;
import javax.ws.rs.Path;


/**
 *
 * @author marx
*/
@Extension(ModuleLifeCycleExtension.class)
public class ContentEngineModuleLifeCycle extends ModuleLifeCycleExtension {

	public static ContentDBImpl DB;

	@Override
	public void activate() {
		ContentEngineManager manager = ContentEngineManager.builder().build();

		manager.getContext().mode = Mode.LOCAL;

		File tempDir = new File(configuration.getDataDir(), "content");
		
		manager.getContext().getConfiguration().put(LocalContentStore.CONFIG_DATADIR, tempDir.getAbsolutePath());

		ContentEngineModuleLifeCycle.DB = manager.getAdDB();
		try {
			ContentEngineModuleLifeCycle.DB.open();
			
			getContext().serviceRegistry().register(ContentDB.class, ContentEngineModuleLifeCycle.DB);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void deactivate() {
		getContext().serviceRegistry().unregister(ContentDBImpl.class, ContentEngineModuleLifeCycle.DB);
	}

	@Override
	public void init() {

	}

	
	
	
	
}

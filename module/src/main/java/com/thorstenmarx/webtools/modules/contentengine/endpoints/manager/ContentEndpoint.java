/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.endpoints.manager;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.thorstenmarx.webtools.api.entities.Store;
import com.thorstenmarx.webtools.api.entities.criteria.Restrictions;
import com.thorstenmarx.webtools.modules.contentengine.beans.Experience;
import com.thorstenmarx.webtools.modules.contentengine.beans.Experiences;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDB;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDBImpl;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.KeyValue;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.DayConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.KeyValueConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.condition.ValidFromToConditionDefinition;
import com.thorstenmarx.webtools.modules.contentengine.definition.impl.ContentDefinitionImpl;
import com.thorstenmarx.webtools.modules.contentengine.enums.ConditionDefinitions;
import com.thorstenmarx.webtools.modules.contentengine.enums.Day;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
@Path("/")
public class ContentEndpoint {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ContentEndpoint.class);
	
	public static final String KEY_CONTENT = "content";

	private final Store<Experience> store;
	private final ContentDB db;

	public ContentEndpoint(final Store<Experience> store, final ContentDB db) {
		this.store = store;
		this.db = db;
	}

	@GET
	@Path("/info")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public String info() {
		return "info";
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Experiences list(final @QueryParam("page") String page) {
		List<Experience> experiences = store.criteria().add(Restrictions.EQ.eq("page", page)).query();
		return new Experiences(page, experiences);
	}

	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Experiences list(final Experiences experiences) {
		try {
			// bei bedarf müssen hier die im Frontend gelöschten Experiences manuell gelöscht werden
			store.save(experiences.getExperiences());
			
			List<String> newIDs = experiences.getExperiences().stream().map(Experience::getId).collect(Collectors.toList());
			Experiences existingExperiences = list(experiences.getPage());
			existingExperiences.getExperiences().stream().filter(exp -> !newIDs.contains(exp.getId())).forEach((exp) -> {
				store.delete(exp);
				try {
					db.delete(exp.getId());
				} catch (IOException ex) {
					LOGGER.error("", ex);
				}
			});
			
			experiences.getExperiences().forEach(this::addContentDefinitionsUncheckedException);
			((ContentDBImpl)db).reopen();
			return experiences;
		} catch (IOException ex) {
			LOGGER.error("", ex);
			throw new WebApplicationException("Error adding experience", Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String add(final Experience content) {
		if (Strings.isNullOrEmpty(content.getType())) {
			throw new WebApplicationException("Type must not be empty", Response.Status.BAD_REQUEST);
		}
		final String id = store.save(content);
		ContentDefinition contentDefinition = ContentDefinitionImpl.forType(content.getType());
		contentDefinition.setId(id);

		JSONObject result = new JSONObject();
		result.put("id", id);
		return result.toJSONString();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(final Experience content) {
		if (Strings.isNullOrEmpty(content.getType())) {
			throw new WebApplicationException("Type must not be empty", Response.Status.BAD_REQUEST);
		}
		store.save(content);
		try {
			addContentDefinitions(content);
		} catch (IOException ex) {
			throw new WebApplicationException("Index could not be updated", Response.Status.INTERNAL_SERVER_ERROR);
		}

	}

	private void addContentDefinitionsUncheckedException (final Experience experience) {
		try {
			addContentDefinitions(experience);
		} catch (IOException ex) {
			throw new WebApplicationException("Index could not be updated", Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	private void addContentDefinitions(final Experience experience) throws IOException {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

		ContentDefinition contentDefinition = ContentDefinitionImpl.forType(experience.getType());
		contentDefinition.setId(experience.getId());
		
		JSONObject jsonContent = new JSONObject();
		JSONArray contentArray = new JSONArray();
		experience.getVariants().forEach((it) -> {
			JSONObject variant = new JSONObject();
			variant.put("content", it.getContent());
			variant.put("editor", it.getEditor());
			variant.put("name", it.getName());
			variant.put("selector", it.getSelector());
			
			contentArray.add(variant);
		});
		jsonContent.put("variants", contentArray);
		contentDefinition.addContent(KEY_CONTENT, jsonContent.toJSONString());

		if (experience.getFrom() != null && experience.getTo() != null) {
			final String from = dateFormat.format(experience.getFrom()) + "0000";
			final String to = dateFormat.format(experience.getTo()) + "2400";
			ValidFromToConditionDefinition sdef = new ValidFromToConditionDefinition();
			sdef.setPeriod(from, to);
			contentDefinition.addConditionDefinition(ConditionDefinitions.VALIDFROMTO, sdef);
		}
		if (experience.getDays() != null && !experience.getDays().isEmpty()) {
			DayConditionDefinition daydef = new DayConditionDefinition();
			experience.getDays().stream().map((Function<String, Day>) (String name) -> {
				switch (name) {
					case "mon":
						return Day.Monday;
					case "tue":
						return Day.Tuesday;
					case "wed":
						return Day.Wednesday;
					case "thu":
						return Day.Thursday;
					case "fri":
						return Day.Friday;
					case "sat":
						return Day.Saturday;
					case "sun":
						return Day.Sunday;
					default:
						throw new IllegalArgumentException("unknow day: " + name);
				}
			}).forEach(daydef::addDay);
			contentDefinition.addConditionDefinition(ConditionDefinitions.DAY, daydef);
		}
		
		if (experience.getSegments() != null &&  !experience.getSegments().isEmpty()) {
			KeyValueConditionDefinition kvdef = new KeyValueConditionDefinition();
			experience.getSegments().stream().map((segment) -> {
				return new KeyValue("segment", segment);
			}).forEach(kvdef::addKeyValue);
			contentDefinition.addConditionDefinition(ConditionDefinitions.KEYVALUE, kvdef);
		}
		
		if (experience.getKeyvalue() != null && !experience.getKeyvalue().isEmpty()) {
			KeyValueConditionDefinition kvdef = new KeyValueConditionDefinition();
			experience.getKeyvalue().forEach((kv) -> {
				kv.getValues().stream().map((segment) -> {
				return new KeyValue(kv.getKey(), segment);
			}).forEach(kvdef::addKeyValue);
			});
			contentDefinition.addConditionDefinition(ConditionDefinitions.KEYVALUE, kvdef);
		}
		
		db.delete(contentDefinition.getId());
		db.add(contentDefinition);
	}

}

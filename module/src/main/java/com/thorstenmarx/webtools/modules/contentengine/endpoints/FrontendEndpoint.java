/**
 * webTools-contentengine
 * Copyright (C) 2016  Thorsten Marx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.modules.contentengine.endpoints;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.thorstenmarx.webtools.modules.contentengine.endpoints.manager.*;
import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import com.thorstenmarx.webtools.api.datalayer.DataLayer;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import com.thorstenmarx.webtools.api.entities.Entities;
import com.thorstenmarx.webtools.api.entities.Store;
import com.thorstenmarx.webtools.api.extensions.RestResourceExtension;
import com.thorstenmarx.webtools.modules.contentengine.beans.Experience;
import com.thorstenmarx.webtools.modules.contentengine.db.ContentDB;
import com.thorstenmarx.webtools.modules.contentengine.db.request.ContentRequest;
import com.thorstenmarx.webtools.modules.contentengine.definition.ContentDefinition;
import com.thorstenmarx.webtools.modules.contentengine.enums.Day;
import com.thorstenmarx.webtools.modules.contentengine.module.ContentEngineModuleLifeCycle;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
@Path("/")
@Extension(RestResourceExtension.class)
public class FrontendEndpoint extends RestResourceExtension {

	private static final Logger LOGGER = LoggerFactory.getLogger(FrontendEndpoint.class);
	
	@Inject
	private Entities entities;
	@Inject
	private AnalyticsDB analyticsDB;
	@Inject
	private DataLayer datalayer;
	
	private Store<Experience> store;
	private ContentDB contentDB;
	
	final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
	final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmm");
	
	private Cache<String, List<ContentDefinition>> contentCache;
	
	@Override
	public void init() {
		store = entities.store(Experience.class);
		contentDB = ContentEngineModuleLifeCycle.DB;//getContext().serviceRegistry().single(ContentDB.class).get();
		
		contentCache = CacheBuilder.newBuilder()
				.expireAfterWrite(1, TimeUnit.MINUTES)
				.build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("content")
	public String content (final @QueryParam("page") String page, final @QueryParam("uid") String uid, final @QueryParam("type") String type) {
		/**
		 * 1. current day
		 * 2. user segments
		 * 3. validfrom / validto
		 * 
		 */
		LocalDateTime ld = LocalDateTime.now();		
		DayOfWeek dow = ld.getDayOfWeek();
		
		ContentRequest request = new ContentRequest();
		
		Optional<SegmentData> segOptional = datalayer.get(uid, SegmentData.KEY, SegmentData.class);
		if (segOptional.isPresent()) {
			SegmentData metaData = segOptional.get();
			Set<String> segmentSet = metaData.getSegments();
			if (segmentSet != null) {
				segmentSet.forEach((segment) -> {
					request.addKeyValue("segment", segment);
				});
			}
		}

		
		request.day(Day.forDayOfWeek(dow));
		request.date(dateFormatter.format(ld));
		request.time(timeFormatter.format(ld));
		request.types(Arrays.asList(type));

		try {
			List<ContentDefinition> result = contentCache.getIfPresent(request.cacheKey());
			if (result == null) {
				result = contentDB.search(request);
			}
			
			JSONObject jsonResult = new JSONObject();
			
			JSONArray contentArray = new JSONArray();
			result.forEach((it) -> {
				final String content = (String) it.getContent(ContentEndpoint.KEY_CONTENT);
				contentArray.add(JSONObject.parse(content));
			});
			
			jsonResult.put("content", contentArray);
			
			return jsonResult.toJSONString();
		} catch (IOException ex) {
			LOGGER.error("", ex);
		}
		
		return "info";
	}
	
}
